La solucion realizada es bastante simple, era requisito construir el backend como un API REST, para esto se hizo uso de ExpressJs.
Se crearon controladores y un modelo , en el segundo los datos utilizados son ficticios y no consultados a una base de datos real.

El backend fue desarrollado usando Express 4 y el front usando ReactJs
Por como fue estructurado, para ejecutar el sistema, deben ser levantados los dos serividores de manera independiente

Para el del backend : 1) moverse hasta la carpeta rentappback , luego npm install, luego npm start
Para el front : 1) moverse hasta la carpeta rentappfront , luego npm install, luego npm start

Alcanzado:
1) creacion del API REST (sin uso de un token)
2) creacion de una interfaz sencilla usando los datos devueltos por el servidor
3) Implementacion de las reglas de negocio

No Alcanzado:
1) test automatizados
2) dise;o mas completo y elaborado del front
3) finalizar proceso de alquiler de una propiedad

Tiempo total dedicado:
8 hrs
