var express = require('express');
var router = express.Router();
var apartment = require('../controllers/apartmentController');

// returns a list of apartaments
router.get('/', function(req, res, next) {
  res.send(apartment.list_all_apartments());
});

// create a new apartament
router.post('/', function(req, res, next) {
  res.json({response: apartment.create_an_apartment()});
});

// get an apartment
router.get('/:apartmentId', function(req, res, next) {
  res.json({response: apartment.get_an_apartment()});
});

// get an apartment
router.put('/:apartmentId', function(req, res, next) {
  res.json({response: apartment.update_an_apartment()});
});

// get an apartment
router.delete('/:apartmentId', function(req, res, next) {
  res.json({response: apartment.delete_an_apartment()});
});


module.exports = router;
