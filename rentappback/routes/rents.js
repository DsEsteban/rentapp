var express = require('express');
var router = express.Router();
var rent = require('../controllers/rentController');

// returns a list of rents
router.get('/', function(req, res, next) {
  res.json({response: rent.list_all_rents()});
});

// create a new rent
router.post('/', function(req, res, next) {

  var userId = req.body.userId,
      apartmentId = req.body.apartmentId,
      days = req.body.days;
      rent.create_a_rent(req, res);
      res.json({response: "done"});
});

// get an rent
router.get('/:rentId', function(req, res, next) {
  res.json({response: rent.get_a_rent()});
});

// update a rent
router.put('/:rentId', function(req, res, next) {
  res.json({response: rent.update_a_rent()});
});

// delete a rent
router.delete('/:rentId', function(req, res, next) {
  res.json({response: rent.delete_a_rent()});
});


module.exports = router;
