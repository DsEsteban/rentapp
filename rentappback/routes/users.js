var express = require('express');
var router = express.Router();
var user = require('../controllers/userController');

// returns a list of apartaments
router.get('/', function(req, res, next) {
  res.json({response: user.list_all_user()});
});

// create a new apartament
router.post('/', function(req, res, next) {
  res.json({response: user.create_an_user()});
});

// get an apartment
router.get('/:apartmentId', function(req, res, next) {
  res.json({response: user.get_an_user()});
});

// get an apartment
router.put('/:apartmentId', function(req, res, next) {
  res.json({response: user.update_an_user()});
});

// get an apartment
router.delete('/:apartmentId', function(req, res, next) {
  res.json({response: user.delete_an_user()});
});


module.exports = router;
