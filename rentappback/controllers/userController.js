'use strict';

// list all apartments
exports.list_all_users = function(req, res){
  return 'all apartments';
}

// get an apartament
exports.get_an_user = function(req, res){
  return 'get an user';
}

// create an apartament
exports.create_an_user = function(req, res){
  return 'create an user';
}

// update an apartment
exports.update_an_user = function(req, res){
  return 'update an user';
}

// delete an apartament
exports.delete_an_user = function(req, res){
  return 'delete an user';
}
