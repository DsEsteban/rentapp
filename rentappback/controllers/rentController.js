'use strict';
var rent = require('../model/rentModel.js')

// list all rents
exports.list_all_rents = function(req, res){
  return 'all rents';
}

// get a rent
exports.get_a_rent = function(req, res){

  return 'get a rent';
}

// create a rent
exports.create_a_rent = function(req, res){
  var rent1 = new rent(req.body.userId, req.body.apartmentId, req.body.days);
  console.log(rent1);
  //rent1.save();
  //res.status(200).send();
  return 'rent added succesfully!'
}

// update a rent
exports.update_a_rent = function(req, res){
  return 'update a rent';
}

// delete a rent
exports.delete_a_rent = function(req, res){
  return 'delete a rent';
}
