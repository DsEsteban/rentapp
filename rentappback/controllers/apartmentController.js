'use strict';
var apartment = require('../model/apartmentModel.js');

// list all apartments
exports.list_all_apartments = function(req, res){
  var apartment1 = new apartment();
  return JSON.parse(apartment1.findAll());
}

// get an apartament
exports.get_an_apartment = function(req, res){
  return 'get an apartment';
}

// create an apartament
exports.create_an_apartment = function(req, res){
  return 'create an apartment';
}

// update an apartment
exports.update_an_apartment = function(req, res){
  return 'update an apartment';
}

// delete an apartament
exports.delete_an_apartment = function(req, res){
  return 'delete an apartment';
}
