'use strict';

module.exports = function(name ="", location = "", ambientesNumber="", squaresMeters="", valueForNight="", valueForMonth="") {
    this.name = name;
    this.location = location;
    this.ambientesNumber = ambientesNumber;
    this.squaresMeters = squaresMeters;
    this.valueForNight = valueForNight;
    this.valueForMonth = valueForMonth;

    this.findAll = function(){
      return JSON.stringify(
        [
          {name:"apto1", location:"av.123", ambientesNumber:3, squaresMeters:70, valueForNight:'5$', valueForMontht:'50$', image:'https://media.equityapartments.com/images/c_crop,x_0,y_0,w_1920,h_1080/c_fill,w_1920,h_1080/q_80/4105-6/2201-wilson-apartments-building.jpg'},
          {name:"apto2", location:"av.456", ambientesNumber:4, squaresMeters:90, valueForNight:'15$', valueForMontht:'70$', image:'https://media.equityapartments.com/images/c_crop,x_0,y_0,w_1920,h_1080/c_fill,w_1920,h_1080/q_80/4105-6/2201-wilson-apartments-building.jpg'},
          {name:"apto3", location:"av.789", ambientesNumber:5, squaresMeters:110, valueForNight:'25$', valueForMontht:'90$', image:'https://media.equityapartments.com/images/c_crop,x_0,y_0,w_1920,h_1080/c_fill,w_1920,h_1080/q_80/4105-6/2201-wilson-apartments-building.jpg'}
        ]
      )
    }
}
