'use strict';
var apartment = require('../model/apartmentModel.js');
var user = require('../model/userModel.js');

module.exports = function(userId, apartmentId, days, rentByNight, rentByMonth){
  this.userId = userId;
  this.apartmentId = apartmentId;
  this.days = days;
  this.cost = (function( days ){

    var user1 = new user();

    let numberOfNights  = days % 30 ;
    let numOfMonths     = Math.floor(days/30);
    let totalValue      = (rentByNight * numberOfNights) + (rentByMonth * numOfMonths);

    if ( 4 < days && days < 15) {
      consle.log('opt1');
      return totalValue*0.05*( user1.find(userId) ? 0.05 : 1 );
    } else if (14 < days) {
      consle.log('opt2');
      return totalValue*0.15*( user1.find(userId) ? 0.05 : 1 );;
    } else {
      consle.log('opt3');
      return totalValue*( user1.find(userId) ? 0.05 : 1 );;
    }

  })(days);
}
