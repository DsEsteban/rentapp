import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  constructor(props){
    super(props);

    this.state = {
      data: [],
      isLoading: false,
      error: null,
    }
  }
  
  componentDidMount(){
    this.setState({ isLoading: true });

    fetch('/apartments')
    .then(res => res.json())
    .then(res => (this.setState({data:res, isLoading: false})))
    .catch(error => this.setState({error, isLoading:false}))
  }

// apartments
  render() {
    const { data, isLoading } = this.state;

    if (isLoading) {
      return <p>Loading...</p>
    }

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="cards-container">
          {data.map(val =>
              <div className="card" style={ { backgroundImage: "url("+val.image+")" } }>
                <p>{val.name}</p>
              </div>
          )}
        </div>
      </div>
    );
  }
}

export default App;
